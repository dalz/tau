-- TODO? golden ratio, tan/atan

-- pieces stored in programmers and CADs are in a different format

tau.pieces = {
  {id = "empty", category = "Special", type = "none"},
  {id = "connector", category = "Special", type = "connector",
    inputs = {{name = "Target", type = "any"}},
    name = "Connector", desc = "Connects pieces"
  },
  -- {id = "error_catcher", category = "Special", type = "none",
  --   inputs = {
  --     {name = "Target", type = "any"},
  --     {name = "Fallback", type = "any"}},
  --   name = "Error Catcher", desc = "Replaces errors in Target with Fallback"
  -- },
  {id = "error_suppressor", category = "Special", type = "none",
    name = "Error Suppressor", desc = "Add this anywhere to suppress all errors"
  },
  {id = "trick_debug", category = "Special", type = "trick",
    inputs = {
      {name = "Target", type = "any"},
      {name = "Number", type = "number", optional = true}},
    name = "Trick: Debug", desc = "Prints Target to the caster's chat, useful for testing"
  },
  {id = "trick_evaluate", category = "Special", type = "trick",
    inputs = {{name = "Target", type = "any"}},
    name = "Trick: evaluate", desc = "Evaluates Target. Useful with Trick: Sleep"},
  {id = "trick_sleep", category = "Special", type = "trick",
    inputs = {{name = "Seconds", type = "number"}},
    name = "Trick: Sleep", desc = "Pauses the execution of the spell for a certain number of seconds"
  },

  {id = "constant_number", category = "Constants", type = "number", val = 0,
    name = "Constant: Number", desc = "An arbitrary number"
  },
  {id = "constant_e", category = "Constants", type = "number", val = 2.718281828459045,
    name = "Constant: e", desc = "Euler's number (2.71828...)"
  },
  {id = "constant_pi", category = "Constants", type = "number", val = math.pi,
    name = "Constant: pi", desc = "3.14159..."
  },
  {id = "constant_tau", category = "Constants", type = "number", val = 2 * math.pi,
    name = "Constant: tau", desc = "2π = 6.28318..."
  },

  {id = "operator_absolute", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Absolute", desc = "Returns the absolute value of Number"
  },
  {id = "operator_acos", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Arc Cosine", desc = "Returns A such that cos(A) = Number"
  },
  {id = "operator_asin", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Arc Sine", desc = "Returns A such that sin(A) = Number"
  },
  {id = "operator_ceiling", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Ceiling", desc = "Rounds Number up"
  },
  {id = "operator_cos", category = "Operators", type = "number",
    inputs = {{name = "Angle", type = "number"}},
    name = "Operator: Cosine", desc = "Returns cos(Angle)"
  },
  {id = "operator_cube", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Cube", desc = "Returns Number³"
  },
  {id = "operator_divide", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Divide", desc = "Returns A / B (/ C)"
  },
  {id = "operator_extract_sign", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Extract Sign", desc = "Extracts Number's sign, returning 1, 0, or -1"
  },
  {id = "operator_floor", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Floor", desc = "Rounds Number down"
  },
  -- {id = "operator_gamma_function", category = "Operators", name = "Operator: gamma function"},
  {id = "operator_integer_divide", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Integer Divide", desc = "Returns A / B (/ C), truncated to integer"
  },
  {id = "operator_inverse", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Inverse", desc = "Returns 1/Number"
  },
  {id = "operator_log", category = "Operators", type = "number",
    inputs = {
      {name = "Number", type = "number"},
      {name = "Base", type = "number", optional = true}},
    name = "Operator: Logarithm", desc = "Returns log(Number). Base defaults to 10"
  },
  {id = "operator_max", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Max", desc = "Returns the largest of the inputs"
  },
  {id = "operator_min", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Min", desc = "Returns the smallest of the inputs"
  },
  {id = "operator_modulus", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"}},
    name = "Operator: Modulus", desc = "Returns A % B"
  },
  {id = "operator_multiply", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Multiply", desc = "Returns A * B (* C)"
  },
  {id = "operator_power", category = "Operators", type = "number",
    inputs = {
      {name = "Base", type = "number"},
      {name = "Exponent", type = "number"}},
    name = "Operator: Power", desc = "Returns Base^Exponent"
  },
  {id = "operator_random", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Random", desc = "Returns a random integer between 0 and Number (exclusive)"
  },
  {id = "operator_root", category = "Operators", type = "number",
    inputs = {
      {name = "Number", type = "number"},
      {name = "Root", type = "number"}},
    name = "Operator: Root", desc = "Returns the nth root of Number"
  },
  {id = "operator_round", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Round", desc = "Rounds Number to an integer (half up)"
  },
  {id = "operator_sin", category = "Operators", type = "number",
    inputs = {{name = "Angle", type = "number"}},
    name = "Operator: Sine", desc = "Returns sin(Angle)"
  },
  {id = "operator_square", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Square", desc = "Returns Number²"
  },
  {id = "operator_square_root", category = "Operators", type = "number",
    inputs = {{name = "Number", type = "number"}},
    name = "Operator: Square Root", desc = "Returns √Number"
  },
  {id = "operator_subtract", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Subtract", desc = "Returns A - B (- C)"
  },
  {id = "operator_sum", category = "Operators", type = "number",
    inputs = {
      {name = "Number A", type = "number"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Sum", desc = "Returns A + B (+ C)"
  },

  -- {id = "operator_node_comparator_strength", category = "Operators", name = "Operator: node comparator strength"},
  {id = "operator_node_hardness", category = "Operators", type = "number",
    inputs = {{name = "Position", type = "vector"}},
    name = "Operator: Node Hardness", description = "Returns the smallest hardness value for the node at Position"
  },
  {id = "operator_node_light", category = "Operators", type = "number",
    inputs = {{name = "Position", type = "vector"}},
    name = "Operator: Node Light", description = "Returns the light value for a light emitting block"
  },
  -- {id = "operator_node_mining_level", category = "Operators", name = "Operator: node mining level"},
  -- {id = "operator_node_side_solidity", category = "Operators", name = "Operator: node side solidity"},
  -- {id = "operator_closest_to_line", category = "Operators", name = "Operator: closest to line"},
  -- {id = "operator_closest_to_point", category = "Operators", name = "Operator: closest to point"},
  {id = "operator_entity_axial_look", category = "Operators", type = "vector",
    inputs = {{name = "Target", type = "entity"}},
    name = "Operator: Entity Axial Look", desc = "Returns Target's look vector, aligned to an axis"
  },
  -- {id = "operator_entity_health", category = "Operators", name = "Operator: entity health"},
  -- {id = "operator_entity_height", category = "Operators", name = "Operator: entity height"},
  {id = "operator_entity_look", category = "Operators", type = "vector",
    inputs = {{name = "Target", type = "entity"}},
    name = "Operator: Entity Look", desc = "Returns Target's look vector"
  },
  -- {id = "operator_entity_motion", category = "Operators", name = "Operator: entity motion"},
  {id = "operator_entity_position", category = "Operators", type = "vector",
    inputs = {{name = "Target", type = "entity"}},
    name = "Operator: Entity Position", desc = "Returns Target's position"
  },
  {id = "operator_entity_raycast", category = "Operators", type = "entity",
    inputs = {
      {name = "Position", type = "vector"},
      {name = "Ray",      type = "vector"},
      {name = "Max",      type = "number", optional = true}},
    name = "Operator: Entity Raycast", desc = "Same as Operator: Vector Raycast, but returns the first entity found"
  },
  -- {id = "operator_focused_entity", category = "Operators", name = "Operator: focused entity"},
  -- {id = "operator_list_add", category = "Operators", name = "Operator: list add"},
  -- {id = "operator_list_exclusion", category = "Operators", name = "Operator: list exclusion"},
  -- {id = "operator_list_index", category = "Operators", name = "Operator: list index"},
  -- {id = "operator_list_intersection", category = "Operators", name = "Operator: list intersection"},
  -- {id = "operator_list_remove", category = "Operators", name = "Operator: list remove"},
  -- {id = "operator_list_size", category = "Operators", name = "Operator: list size"},
  -- {id = "operator_list_union", category = "Operators", name = "Operator: list union"},
  -- {id = "operator_random_entity", category = "Operators", name = "Operator: random entity"},

  {id = "operator_planar_normal_vector", category = "Operators", type = "vector",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Planar Normal Vector", desc = ""
  },
  {id = "operator_vector_absolute", category = "Operators", type = "vector",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Absolute", desc = "Returns a vector made by taking the absolute value of each input component"
  },
  {id = "operator_vector_construct", category = "Operators", type = "vector",
    inputs = {
      {name = "X", type = "number", optional = true},
      {name = "Y", type = "number", optional = true},
      {name = "Z", type = "number", optional = true}},
    name = "Operator: Vector Construct", desc = "Create a new vector (components default to 0 if not set)"
  },
  {id = "operator_vector_cross_product", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Vector B", type = "vector"}},
    name = "Operator: Vector Cross Product", desc = "A ⨯ B"
  },
  {id = "operator_vector_divide", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Vector Divide", desc = "A / B"
  },
  {id = "operator_vector_dot_product", category = "Operators", type = "number",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Vector B", type = "vector"}},
    name = "Operator: Vector Dot Product", desc = "A · B"
  },
  {id = "operator_vector_extract_sign", category = "Operators", type = "vector",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Extract Sign", desc = "Returns a vector whose components are the sign of each input component"
  },
  {id = "operator_vector_extract_x", category = "Operators", type = "number",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Extract X", desc = "Returns Vector's X component"
  },
  {id = "operator_vector_extract_y", category = "Operators", type = "number",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Extract Y", desc = "Returns Vector's Y component"
  },
  {id = "operator_vector_extract_z", category = "Operators", type = "number",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Extract Z", desc = "Returns Vector's Z component"
  },
  {id = "operator_vector_magnitude", category = "Operators", type = "number",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Magnitude", desc = "Returns Vector's length"
  },
  {id = "operator_vector_multiply", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Number B", type = "number"},
      {name = "Number C", type = "number", optional = true}},
    name = "Operator: Vector Multiply", desc = "A * B (* C)"
  },
  {id = "operator_vector_negate", category = "Operators", type = "vector",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Negate", desc = "Negates Vector"
  },
  {id = "operator_vector_normalize", category = "Operators", type = "vector",
    inputs = {{name = "Vector", type = "vector"}},
    name = "Operator: Vector Normalize", desc = "Returns a unit vector with Vector's direction"
  },
  {id = "operator_vector_piecewise_maximum", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Vector B", type = "vector"},
      {name = "Vector C", type = "vector", optional = true}},
    name = "Operator: Vector Piecewise Maximum", desc = "Constructs a vector by taking the biggest X, Y and Z component from the inputs"
  },
  {id = "operator_vector_piecewise_minimum", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Vector B", type = "vector"},
      {name = "Vector C", type = "vector", optional = true},},
    name = "Operator: Vector Piecewise Minimum", desc = "Constructs a vector by taking the smallest X, Y and Z component from the inputs"
  },
  {id = "operator_vector_project", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Vector B", type = "vector"},},
    name = "Operator: Vector Project", desc = "Returns the projection of Vector A on Vector B"
  },
  {id = "operator_vector_raycast", category = "Operators", type = "vector",
    inputs = {
      {name = "Position", type = "vector"},
      {name = "Ray",      type = "vector"},
      {name = "Max",      type = "number", optional = true}},
    name = "Operator: Vector Raycast", desc = "Returns the position of the first non liquid node encountered by tracing a ray starting from Position with direction Ray. If nothing is found within a distance of min(Max, 64) an error is signaled"
  },
  {id = "operator_vector_raycast_axis", category = "Operators", type = "vector",
    inputs = {
      {name = "Position", type = "vector"},
      {name = "Ray",      type = "vector"},
      {name = "Max",      type = "number", optional = true}},
    name = "Operator: Vector Raycast Axis", desc = "Same as Operator: Vector Raycast, but returns a unit axis vector indicating which of the node's sides was hit by the ray"
  },
  {id = "operator_vector_rotate", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector", type = "vector"},
      {name = "Axis",   type = "vector"},
      {name = "Angle",  type = "number"}},
    name = "Operator: Vector Rotate", desc = "Rotates Vector along Axis by Angle"
  },
  {id = "operator_vector_subtract", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Vector B", type = "vector"},
      {name = "Vector C", type = "vector", optional = true},},
    name = "Operator: Vector Subtract", desc = "A - B (- C)"
  },
  {id = "operator_vector_sum", category = "Operators", type = "vector",
    inputs = {
      {name = "Vector A", type = "vector"},
      {name = "Vector B", type = "vector"},
      {name = "Vector C", type = "vector", optional = true},},
    name = "Operator: Vector Sum", desc = "A + B (+ C)"
  },

  -- {id = "selector_attacker", category = "Selectors", name = "Selector: attacker"},
  -- {id = "selector_attack_target", category = "Selectors", name = "Selector: attack target"},
  -- {id = "selector_node_broken", category = "Selectors", name = "Selector: node broken"},
  -- {id = "selector_node_presence", category = "Selectors", name = "Selector: node presence"},
  -- {id = "selector_node_side_broken", category = "Selectors", name = "Selector: node side broken"},
  {id = "selector_caster", category = "Selectors", type = "entity",
    name = "Selector: Caster", desc = "Returns the player casting the spell"
  },
  -- {id = "selector_caster_battery", category = "Selectors", name = "Selector: caster battery"},
  -- {id = "selector_caster_energy", category = "Selectors", name = "Selector: caster energy"},
  -- {id = "selector_damage_taken", category = "Selectors", name = "Selector: damage taken"},
  -- {id = "selector_eidos_changelog", category = "Selectors", name = "Selector: eidos changelog"},
  -- {id = "selector_focal_point", category = "Selectors", name = "Selector: focal point"},
  -- {id = "selector_is_elytra_flying", category = "Selectors", name = "Selector: is elytra flying"},
  -- {id = "selector_item_count", category = "Selectors", name = "Selector: item count"},
  -- {id = "selector_item_presence", category = "Selectors", name = "Selector: item presence"},
  -- {id = "selector_loopcast_index", category = "Selectors", name = "Selector: loopcast index"},
  -- {id = "selector_nearby_animals", category = "Selectors", name = "Selector: nearby animals"},
  -- {id = "selector_nearby_charges", category = "Selectors", name = "Selector: nearby charges"},
  -- {id = "selector_nearby_enemies", category = "Selectors", name = "Selector: nearby enemies"},
  -- {id = "selector_nearby_falling_nodes", category = "Selectors", name = "Selector: nearby falling nodes"},
  -- {id = "selector_nearby_glowing", category = "Selectors", name = "Selector: nearby glowing"},
  -- {id = "selector_nearby_items", category = "Selectors", name = "Selector: nearby items"},
  -- {id = "selector_nearby_living", category = "Selectors", name = "Selector: nearby living"},
  -- {id = "selector_nearby_players", category = "Selectors", name = "Selector: nearby players"},
  -- {id = "selector_nearby_projectiles", category = "Selectors", name = "Selector: nearby projectiles"},
  -- {id = "selector_nearby_smeltables", category = "Selectors", name = "Selector: nearby smeltables"},
  -- {id = "selector_nearby_vehicles", category = "Selectors", name = "Selector: nearby vehicles"},
  -- {id = "selector_ruler_vector", category = "Selectors", name = "Selector: ruler vector"},
  -- {id = "selector_saved_vector", category = "Selectors", name = "Selector: saved vector"},
  -- {id = "selector_sneak_status", category = "Selectors", name = "Selector: sneak status"},
  -- {id = "selector_sucession_counter", category = "Selectors", name = "Selector: sucession counter"},
  -- {id = "selector_time", category = "Selectors", name = "Selector: time"},
  -- {id = "selector_transmission", category = "Selectors", name = "Selector: transmission"},
  -- {id = "spell_nodes", name = "spell nodes"},

  {id = "trick_add_motion", category = "Tricks", type = "trick",
    inputs = {
      {name = "Target",    type = "entity"},
      {name = "Direction", type = "vector"},
      {name = "Speed",     type = "number"}},
    name = "Trick: Add Motion", desc = "Adds Speed along Direction to Target"
  },
  {id = "trick_blaze", category = "Tricks", type = "trick",
    inputs = {{name = "Position", type = "vector"}},
    name = "Trick: Blaze", desc = "Creates fire at the given point"
  },
  {id = "trick_blink", category = "Tricks", type = "trick",
    inputs = {
      {name = "Target",   type = "entity"},
      {name = "Distance", type = "number"}},
    name = "Trick: Blink", desc = "Teleports Target in the direction it's lookikng"
  },
  -- {id = "trick_break_node", category = "Tricks", name = "Trick: break node"},
  -- {id = "trick_break_in_sequence", category = "Tricks", name = "Trick: break in sequence"},
  -- {id = "trick_break_loop", category = "Tricks", name = "Trick: break loop"},
  -- {id = "trick_broadcast", category = "Tricks", name = "Trick: broadcast"},
  -- {id = "trick_change_slot", category = "Tricks", name = "Trick: change slot"},
  -- {id = "trick_collapse_node", category = "Tricks", name = "Trick: collapse node"},
  -- {id = "trick_collapse_node_sequence", category = "Tricks", name = "Trick: collapse node sequence"},
  -- {id = "trick_conjure_node", category = "Tricks", name = "Trick: conjure node"},
  -- {id = "trick_conjure_node_sequence", category = "Tricks", name = "Trick: conjure node sequence"},
  -- {id = "trick_conjure_light", category = "Tricks", name = "Trick: conjure light"},
  -- {id = "trick_debug_spamless", category = "Tricks", name = "Trick: debug spamless"},
  -- {id = "trick_detonate", category = "Tricks", name = "Trick: detonate"},
  -- {id = "trick_die", category = "Tricks", name = "Trick: die"},
  -- {id = "trick_ebony_ivory", category = "Tricks", name = "Trick: ebony ivory"},
  -- {id = "trick_eidos_anchor", category = "Tricks", name = "Trick: eidos anchor"},
  -- {id = "trick_eidos_reversal", category = "Tricks", name = "Trick: eidos reversal"},
  {id = "trick_explode", category = "Tricks", type = "trick",
    inputs = {
      {name = "Position", type = "vector"},
      {name = "Power",    type = "number"}},
    name = "Trick: Explode", desc = "Creates an explosion of intensity Power at Position"
  },
  -- {id = "trick_fire_resistance", category = "Tricks", name = "Trick: fire resistance"},
  -- {id = "trick_greater_infusion", category = "Tricks", name = "Trick: greater infusion"},
  -- {id = "trick_haste", category = "Tricks", name = "Trick: haste"},
  -- {id = "trick_ignite", category = "Tricks", name = "Trick: ignite"},
  -- {id = "trick_infusion", category = "Tricks", name = "Trick: infusion"},
  -- {id = "trick_invisibility", category = "Tricks", name = "Trick: invisibility"},
  -- {id = "trick_jump_boost", category = "Tricks", name = "Trick: jump boost"},
  -- {id = "trick_mass_add_motion", category = "Tricks", name = "Trick: mass add motion"},
  -- {id = "trick_mass_blink", category = "Tricks", name = "Trick: mass blink"},
  -- {id = "trick_mass_exodus", category = "Tricks", name = "Trick: mass exodus"},
  {id = "trick_move_node", category = "Tricks", type = "trick",
    inputs = {
      {name = "Position", type = "vector"},
      {name = "Axis",     type = "vector"}},
    name = "Trick: Move Node", desc = "Moves the node at Position by 1 along Axis"
  },
  -- {id = "trick_move_node_sequence", category = "Tricks", name = "Trick: move node sequence"},
  -- {id = "trick_night_vision", category = "Tricks", name = "Trick: night vision"},
  -- {id = "trick_overgrow", category = "Tricks", name = "Trick: overgrow"},
  -- {id = "trick_particle_trail", category = "Tricks", name = "Trick: particle trail"},
  -- {id = "trick_place_node", category = "Tricks", name = "Trick: place node"},
  -- {id = "trick_place_in_sequence", category = "Tricks", name = "Trick: place in sequence"},
  -- {id = "trick_play_sound", category = "Tricks", name = "Trick: play sound"},
  -- {id = "trick_regeneration", category = "Tricks", name = "Trick: regeneration"},
  -- {id = "trick_resistance", category = "Tricks", name = "Trick: resistance"},
  -- {id = "trick_russian_roulette", category = "Tricks", name = "Trick: russian roulette"},
  -- {id = "trick_save_vector", category = "Tricks", name = "Trick: save vector"},
  -- {id = "trick_slowness", category = "Tricks", name = "Trick: slowness"},
  -- {id = "trick_smelt_node", category = "Tricks", name = "Trick: smelt node"},
  -- {id = "trick_smelt_node_sequence", category = "Tricks", name = "Trick: smelt node sequence"},
  -- {id = "trick_smelt_item", category = "Tricks", name = "Trick: smelt item"},
  -- {id = "trick_smite", category = "Tricks", name = "Trick: smite"},
  -- {id = "trick_speed", category = "Tricks", name = "Trick: speed"},
  -- {id = "trick_spin_chamber", category = "Tricks", name = "Trick: spin chamber"},
  -- {id = "trick_strength", category = "Tricks", name = "Trick: strength"},
  -- {id = "trick_switch_target_slot", category = "Tricks", name = "Trick: switch target slot"},
  -- {id = "trick_till", category = "Tricks", name = "Trick: till"},
  -- {id = "trick_till_sequence", category = "Tricks", name = "Trick: till sequence"},
  -- {id = "trick_torrent", category = "Tricks", name = "Trick: torrent"},
  -- {id = "trick_water_breathing", category = "Tricks", name = "Trick: water breathing"},
  -- {id = "trick_weakness", category = "Tricks", name = "Trick: weakness"},
  -- {id = "trick_wither", category = "Tricks", name = "Trick: wither"},
}

tau.pieces_by_id = {}
for _, piece in ipairs(tau.pieces) do
  tau.pieces_by_id[piece.id] = piece
end

tau.categories = {"Special", "Constants", "Selectors", "Operators", "Tricks"}
