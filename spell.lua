tau.trick_range = 32

-- get a linear topological order of the bidimensional program
function tau.compile(program)
  local spell = {}

  local function program_get(r, c, dir, rec_depth)
    rec_depth = rec_depth or 0
    if rec_depth > 81 then
      return nil, r, c
    end

    if dir == "n" then
      r = r - 1
    elseif dir == "s" then
      r = r + 1
    elseif dir == "e" then
      c = c + 1
    elseif dir == "w" then
      c = c - 1
    else
      return nil, r, c
    end

    if program[r] and program[r][c] then
      if tau.pieces_by_id[program[r][c].id].type == "connector" then
        local s = program[r][c].ins[1]
        if s then
          return program_get(r, c, s, rec_depth + 1)
        end
      end

      return program[r][c], r, c

    else
      return nil, r, c
    end
  end

  local function add_to_spell(r, c, rec_depth)
    rec_depth = rec_depth or 0
    if rec_depth > 81 then
      return "loop detected at [" .. r .. "," .. c .. "]"
    end

    local p = program[r][c]
    local p_spec = tau.pieces_by_id[p.id]

    if p.loc then
      return nil, p.loc
    end

    -- compiled piece
    local comp = {
      id = p.id,
      ins = {}
    }

    if p.val then
      comp.val = p.val
    elseif p_spec.val then
      comp.val = p_spec.val
    end

    for i, input in ipairs(p_spec.inputs or {}) do
      local side = p.ins and p.ins[i]

      if not side or side == "x" then
        if input.optional then
          goto next
        end

        return "input '" .. input.name ..
          "' at [" .. r .. "," .. c ..  "] is required"
      end

      local inp, inr, inc = program_get(r, c, side)
      local int = inp and tau.pieces_by_id[inp.id].type

      if not int or int == "none" then
        return "empty input '" .. input.name .. "' at [" .. r .. "," .. c .. "]"

      elseif int == "trick" or input.type ~= int and input.type ~= "any" then
        return "input type mismatch at [" .. r .. "," .. c .. "]: expected " ..
          input.type .. ", found " .. int
      end

      local err, loc = add_to_spell(inr, inc, rec_depth + 1)
      if err then
        return err
      end

      comp.ins[i] = loc

      ::next::
    end

    spell[#spell + 1] = comp
    p.loc = #spell

    return nil, p.loc
  end

  for r, row in ipairs(program) do
    for c, piece in ipairs(row) do
      if tau.pieces_by_id[piece.id].type == "trick" then
        local err = add_to_spell(r, c)
        if err then
          return nil, err
        end

      elseif piece.id == "error_suppressor" then
        spell.noerr = true

      -- elseif piece.id == "error_catcher" then
      --   local target, tr, tc = program_get(r, c, piece.ins[1])
      --   local fallback, fr, fc = program_get(r, c, piece.ins[2])
      --   local tp = target and tau.pieces_by_id[target.id]
      --   local fp = fallback and tau.pieces_by_id[fallback.id]

      --   local missing
      --   if not tp or tp.type == "none" then
      --     missing = "Target"
      --   elseif not fp or fp.type == "none" then
      --     missing = "Fallback"
      --   end
      --   if missing then
      --     return nil, "input '" .. missing ..
      --       "' at [" .. r .. "," .. c .. "] is required"
      --   end

      --   if tp.type ~= fp.type then
      --     return nil, "type mismatch between 'Target' and 'Fallback' at [" ..
      --       r .. "," .. c .. "]"
      --   end

      --   -- FIXME totally broken
      --   local err, floc = add_to_spell(fr, fc)
      --   if err then
      --     return nil, err
      --   end

      --   local err, tloc = add_to_spell(tr, tc)
      --   if err then
      --     return nil, err
      --   end

      --   spell[tloc].fb = floc
      end
    end
  end

  return spell, nil
end

local function raycast(pos, ray, max, entities)
  max = math.min(max or math.huge, 2*tau.trick_range)
  local target = vector.add(pos, vector.multiply(vector.normalize(ray), max))
  return minetest.raycast(pos, target, entities or false)
end

tau.exec_table = {
  trick_debug = function(s, p, c)
    local n = p.ins[2] and ("[" .. s[p.ins[2]].val .. "] ") or ""
    local t = s[p.ins[1]].val
    local msg = n

    if type(t) == "userdata" then
      if t:is_player() then
        local pos = t:get_pos()
        msg = msg .. "player '" .. t:get_player_name() .. "' at [" ..
          pos.x .. "," .. pos.y .. "," .. pos.z .. "]"
      else
        msg = msg .. minetest.serialize(t:get_properties())
      end
    else
      msg = msg .. minetest.serialize(t):sub(8) -- skip the 'return '
    end

    minetest.chat_send_player(c:get_player_name(), msg)
  end,

  operator_absolute = function(s, p)
    p.val = math.abs(s[p.ins[1]].val)
  end,

  operator_acos = function(s, p, c)
    p.val = math.acos(s[p.ins[1]].val)

    if p.val ~= p.val then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Arc Cosine")
      end

      return true
    end
  end,

  operator_asin = function(s, p, c)
    p.val = math.asin(s[p.ins[1]].val)

    if p.val ~= p.val then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Arc Sine")
      end

      return true
    end
  end,

  operator_ceiling = function(s, p)
    p.val = math.ceil(s[p.ins[1]].val)
  end,

  operator_cos = function(s, p)
    p.val = math.cos(s[p.ins[1]].val)
  end,

  operator_cube = function(s, p)
    p.val = s[p.ins[1]].val^3
  end,

  operator_divide = function(s, p, c)
    local x, y, z = s[p.ins[1]].val, s[p.ins[2]].val, p.ins[3] and s[p.ins[3]].val or 1

    if y == 0 or z == 0 then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Divide (division by zero)")
      end

      return true
    end

    p.val = x / y / z
  end,

  operator_extract_sign = function(s, p)
    p.val = tau.sign(s[p.ins[1]].val)
  end,

  operator_floor = function(s, p)
    p.val = math.floor(s[p.ins[1]].val)
  end,

  operator_integer_divide = function(s, p, c)
    local x, y, z = s[p.ins[1]].val, s[p.ins[2]].val, p.ins[3] and s[p.ins[3]].val or 1

    if y == 0 or z == 0 then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Integer Divide (division by zero)")
      end

      return true
    end

    local v = x / y / z
    p.val = v > 0 and math.floor(v) or math.ceil(v)
  end,

  operator_inverse = function(s, p, c)
    local n = s[p.ins[1]].val

    if n == 0 then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Inverse (division by zero)")
      end

      return true
    end

    p.val = 1 / n
  end,

  operator_log = function(s, p, c)
    local n, b = s[p.ins[1]].val, p.ins[2] and s[p.ins[2]].val

    if n <= 0 or b and (b <= 0 or b == 1) then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Logarithm")
      end

      return true
    end

    if b then
      p.val = math.log(n) / math.log(b)
    else
      p.val = math.log10(n)
    end
  end,

  operator_max = function(s, p)
    p.val = math.max(s[p.ins[1]].val, s[p.ins[2]].val,
      p.ins[3] and s[p.ins[3]].val or -math.huge)
  end,

  operator_min = function(s, p)
    p.val = math.min(s[p.ins[1]].val, s[p.ins[2]].val,
      p.ins[3] and s[p.ins[3]].val or math.huge)
  end,

  operator_modulus = function(s, p)
    p.val = s[p.ins[1]].val % s[p.ins[2]].val
  end,

  operator_multiply = function(s, p)
    p.val = s[p.ins[1]].val * s[p.ins[2]].val *
      (p.ins[3] and s[p.ins[3]].val or 1)
  end,

  operator_power = function(s, p)
    p.val = s[p.ins[1]].val ^ s[p.ins[2]].val

    if p.val ~= p.val then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Power")
      end

      return true
    end
  end,

  operator_random = function(s, p)
    local n = s[p.ins[1]].val
    p.val = math.random(n) + (n < 0 and 1 or -1)
  end,

  operator_root = function(s, p)
    p.val = s[p.ins[1]].val ^ (1 / s[p.ins[2]].val)

    if p.val ~= p.val then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Root")
      end

      return true
    end

    p.val = math.sqrt(n)
  end,

  operator_round = function(s, p)
    p.val = math.floor(s[p.ins[1]].val + 0.5)
  end,

  operator_sin = function(s, p)
    p.val = math.sin(s[p.ins[1]].val)
  end,

  operator_square = function(s, p)
    p.val = s[p.ins[1]].val^2
  end,

  operator_subtract = function(s, p)
    p.val = s[p.ins[1]].val - s[p.ins[2]].val -
      (p.ins[3] and s[p.ins[3]].val or 0)
  end,

  operator_sum = function(s, p)
    p.val = s[p.ins[1]].val + s[p.ins[2]].val +
      (p.ins[3] and s[p.ins[3]].val or 0)
  end,

  operator_node_hardness = function(s, p)
    local n = minetest.registered_nodes[minetest.get_node(s[p.ins[1]].val).name]

    print(minetest.serialize(n))
    if not (n or {}).groups then
      p.val = math.huge
    elseif n.groups.dig_immediate == 3 then
      p.val = 0
    else
      p.val = math.min(
        n.groups.crumbly or math.huge,
        n.groups.cracky or math.huge,
        n.groups.snappy or math.huge,
        n.groups.choppy or math.huge,
        n.groups.fleshy or math.huge,
        n.groups.oddly_breakable_by_hand or math.huge)
    end
  end,

  operator_node_light = function(s, p)
    local n = minetest.registered_nodes[minetest.get_node(s[p.ins[1]].val).name]
    p.val = (n or {}).light_source or 0
  end,

  operator_square_root = function(s, p)
    local n = s[p.ins[1]].val

    if n < 0 then
      if not s.noerr and not p.fb then
        tau.hud_error(c, "Invalid input for Operator: Square Root (negative number)")
      end

      return true
    end

    p.val = math.sqrt(n)
  end,

  operator_entity_axial_look = function(s, p)
    local t = s[p.ins[1]].val

    if not tau.objref_valid(t) then
      return true
    end

    local look
    if t:is_player() then
      look = t:get_look_dir()
    else
      look = vector.rotate(vector.new(0, 0, 1), t:get_rotation())
    end

    local x, y, z = math.abs(look.x), math.abs(look.y), math.abs(look.z)
    if x >= y and x >= z then
      p.val = vector.new(tau.sign(look.x), 0, 0)
    elseif y > x and y >= z then
      p.val = vector.new(0, tau.sign(look.y), 0)
    else
      p.val = vector.new(0, 0, tau.sign(look.z))
    end
  end,

  operator_entity_look = function(s, p)
    local t = s[p.ins[1]].val

    if not tau.objref_valid(t) then
      return true
    end

    if t:is_player() then
      p.val = t:get_look_dir()
    else
      p.val = vector.rotate(vector.new(0, 0, 1), t:get_rotation())
    end
  end,

  operator_entity_position = function(s, p)
    local t = s[p.ins[1]].val

    if not tau.objref_valid(t) then
      return true
    end

    p.val = t:get_pos()

    if t:is_player() then
      p.val.y = p.val.y + t:get_properties().eye_height
    end
  end,

  operator_entity_raycast = function(s, p, c)
    -- FIXME doesn't work on dropped items in mineclone2
    -- doesn't work in minecraft either?

    local pos, ray, max = s[p.ins[1]].val, s[p.ins[2]].val, p.ins[3] and s[p.ins[3]].val
    for thing in raycast(pos, ray, max, true) do
      if thing.type == "object" and thing.ref ~= c then
        p.val = thing.ref
        return
      end
    end

    if not s.noerr and not p.fb then
      tau.hud_error(c, "No entities found by Operator: Entity Raycast")
    end

    return true
  end,

  operator_planar_normal_vector = function(s, p)
    p.val = vector.rotate_around_axis(s[p.ins[1]].val, vector.new(1, 1, 1), -2/3 * math.pi)
  end,

  operator_vector_absolute = function(s, p)
    p.val = vector.apply(s[p.ins[1]].val, math.abs)
  end,

  operator_vector_construct = function(s, p)
    local x = p.ins[1] and s[p.ins[1]].val or 0
    local y = p.ins[2] and s[p.ins[2]].val or 0
    local z = p.ins[3] and s[p.ins[3]].val or 0

    p.val = vector.new(x, y, z)
  end,

  operator_vector_cross_product = function(s, p)
    p.val = vector.cross(s[p.ins[1]].val, s[p.ins[2]].val)
  end,

  operator_vector_divide = function(s, p)
    p.val = vector.divide(s[p.ins[1]].val, s[p.ins[2]].val)

    if p.ins[3] then
      p.val = vector.divide(p.val, s[p.ins[3]].val)
    end
  end,

  operator_vector_dot_product = function(s, p)
    p.val = vector.dot(s[p.ins[1]].val, s[p.ins[2]].val)
  end,

  operator_vector_extract_sign = function(s, p)
    local v = s[p.ins[1]].val
    p.val = vector.new(tau.sign(v.x), tau.sign(v.y), tau.sign(v.z))
  end,

  operator_vector_extract_x = function(s, p)
    p.val = s[p.ins[1]].val.x
  end,

  operator_vector_extract_y = function(s, p)
    p.val = s[p.ins[1]].val.y
  end,

  operator_vector_extract_z = function(s, p)
    p.val = s[p.ins[1]].val.z
  end,

  operator_vector_magnitude = function(s, p)
    p.val = vector.length(s[p.ins[1]].val)
  end,

  operator_vector_multiply = function(s, p)
    p.val = vector.multiply(s[p.ins[1]].val, s[p.ins[2]].val)

    if p.ins[3] then
      p.val = vector.multiply(p.val, s[p.ins[3]].val)
    end
  end,

  operator_vector_negate = function(s, p)
    p.val = vector.apply(s[p.ins[1]].val, function(n) return -n end)
  end,

  operator_vector_normalize = function(s, p)
    p.val = vector.normalize(s[p.ins[1]].val)
  end,

  operator_vector_piecewise_maximum = function(s, p)
    local a, b = s[p.ins[1]].val, s[p.ins[2]].val
    local c = p.ins[3] and s[p.ins[3]].val or
      vector.new(-math.huge, -math.huge, -math.huge)

    p.val = vector.new(
      math.max(a.x, b.x, c.x),
      math.max(a.y, b.y, c.y),
      math.max(a.z, b.z, c.z))
  end,

  operator_vector_piecewise_minimum = function(s, p)
    local a, b = s[p.ins[1]].val, s[p.ins[2]].val
    local c = p.ins[3] and s[p.ins[3]].val or
      vector.new(math.huge, math.huge, math.huge)

    p.val = vector.new(
      math.min(a.x, b.x, c.x),
      math.min(a.y, b.y, c.y),
      math.min(a.z, b.z, c.z))
  end,

  operator_vector_project = function(s, p)
    local a, b = s[p.ins[1]].val, vector.normalize(s[p.ins[2]].val)
    p.val = vector.multiply(b, vector.dot(a, b))
  end,

  operator_vector_raycast = function(s, p, c)
    local pos, ray, max = s[p.ins[1]].val, s[p.ins[2]].val, p.ins[3] and s[p.ins[3]].val
    for thing in raycast(pos, ray, max) do
      p.val = thing.under
      return
    end

    if not s.noerr and not p.fb then
      tau.hud_error(c, "No nodes found by Operator: Vector Raycast")
    end

    return true
  end,

  operator_vector_raycast_axis = function(s, p, c)
    local pos, ray, max = s[p.ins[1]].val, s[p.ins[2]].val, p.ins[3] and s[p.ins[3]].val
    for thing in raycast(pos, ray, max) do
      p.val = thing.intersection_normal
      return
    end

    if not s.noerr and not p.fb then
      tau.hud_error(c, "No nodes found by Operator: Vector Raycast Axis")
    end

    return true
  end,

  operator_vector_rotate = function(s, p)
    p.val = vector.rotate_around_axis(
      s[p.ins[1]].val, s[p.ins[2]].val, -s[p.ins[3]].val)
  end,

  operator_vector_subtract = function(s, p)
    p.val = vector.subtract(s[p.ins[1]].val, s[p.ins[2]].val)

    if p.ins[3] then
      p.val = vector.subtract(p.val, s[p.ins[3]].val)
    end
  end,

  operator_vector_sum = function(s, p)
    p.val = vector.add(s[p.ins[1]].val, s[p.ins[2]].val)

    if p.ins[3] then
      p.val = vector.add(p.val, s[p.ins[3]].val)
    end
  end,

  selector_caster = function(s, p, c)
    p.val = c
  end,

  trick_add_motion = function(s, p, c, fp)
    local t, d, sp = s[p.ins[1]].val, s[p.ins[2]].val, s[p.ins[3]].val

    if not tau.objref_valid(t) then
      return
    end

    if not tau.trick_in_range(fp, t:get_pos(), c, s.noerr) then
      return true
    end

    -- TODO add_velocity will work with players too in next version
    if t:is_player() then
      t:add_player_velocity(vector.multiply(vector.normalize(d), sp))
    else
      t:add_velocity(vector.multiply(vector.normalize(d), sp))
    end
  end,

  trick_blaze = function(s, p, c, fp)
    local pos = s[p.ins[1]].val

    if not tau.trick_in_range(fp, pos, c, s.noerr) then
      return true
    end

    if minetest.get_node(pos).name == "air" then
      -- TODO depends on MTG
      minetest.set_node(pos, {name = "fire:basic_flame"})
    end
  end,

  trick_blink = function(s, p, c, fp)
    local t, d = s[p.ins[1]].val, s[p.ins[2]].val

    if not tau.objref_valid(t) then
      return
    end

    local pos = t:get_pos()
    if t:is_player() then
      pos.y = pos.y + t:get_properties().eye_height
    end

    local ray
    if t:is_player() then
      ray = t:get_look_dir()
    else
      ray = vector.rotate(vector.new(0, 0, 1), t:get_rotation())
    end

    local dest = vector.add(pos, vector.multiply(ray, d))
    for thing in raycast(pos, ray, d) do
      dest = thing.above
      break
    end

    if t:is_player() then
      local eh = t:get_properties().eye_height
      for thing in raycast(dest, vector.new(0, -1, 0), eh) do
        dest.y = thing.intersection_point.y
        break
      end
    end

    t:set_pos(dest)
  end,

  trick_explode = function(s, p, c, fp)
    local t = s[p.ins[1]].val

    if not tau.trick_in_range(fp, t, c, s.noerr) then
      return true
    end

    -- TODO depends on MTG
    tnt.boom(t, {radius = s[p.ins[2]].val})
  end,

  trick_move_node = function(s, p, c, fp)
    local pos = vector.floor(s[p.ins[1]].val)
    local axis = vector.normalize(s[p.ins[2]].val)

    if not tau.trick_in_range(fp, pos, c, s.noerr) then
      return true
    end

    if not tau.is_axis(axis) then
      return
    end

    local dest = vector.add(pos, axis)
    local dn_name = minetest.get_node(dest).name
    local dn_liquidtype = (minetest.registered_nodes[dn_name] or {}).liquidtype

    if dn_name ~= "air" and (not dn_liquidtype or dn_liquidtype == "none") then
      return
    end

    local meta = minetest.get_meta(pos):to_table()
    minetest.set_node(dest, minetest.get_node(pos))
    minetest.get_meta(dest, meta):from_table(meta)
    minetest.remove_node(pos)
    minetest.check_for_falling(dest)
  end,
}


function tau.execute(spell, caster, start_index)
  if not tau.objref_valid(caster) then
    return
  end
  local focal_point = caster:get_pos()

  for i, piece in ipairs(spell) do
    if start_index and i < start_index then
      goto next
    end

    if piece.id == "trick_sleep" then
      minetest.after(spell[piece.ins[1]].val, tau.execute, spell, caster, i + 1)
      return
    end

    local f = tau.exec_table[piece.id]
    if f and f(spell, piece, caster, focal_point) then
      if piece.fb then
        piece.val = spell[piece.fb].val
      else
        break
      end
    end

    ::next::
  end
end
