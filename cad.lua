minetest.register_craftitem("tau:cad", {
  description = "Casting Assistant Device",
  stack_max = 1,
  inventory_image = "tau_cad_assembly_creative.png",

  on_use = function(itemstack, user, pointed_thing)
    local meta = itemstack:get_meta()
    local program

    if pointed_thing.type == "node" then
      program = minetest.deserialize(
        minetest.get_meta(pointed_thing.under):get_string("tau:program"))
    end

    print(minetest.serialize(program))

    if not program then
      print(meta:get_string("tau:spell"))

      local spell = minetest.deserialize(meta:get_string("tau:spell"))

      if spell then
        tau.execute(spell, user)
      end

      return
    end

    local spell, err = tau.compile(program)

    if err then
      tau.hud_error(user, "Compilation failed: " .. err)
      return
    end

    meta:set_string("tau:spell", minetest.serialize(spell))

    return itemstack
  end,
})
