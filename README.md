# Tau

Tau is a reimplementation for Minetest of the
[Psi Minecraft mod](https://github.com/Vazkii/Psi) by Vazkii.
It enables the creation of spells by using a simple visual programming
environment based on pieces placed on a grid.

## Current status
Creating spells is already possible, and pieces from Psi are gradually being
implemented. Casting has no limits and costs as of now, so only a
non-customizable Creative CAD (which can hold a single spell) is available.

## Known differences from Psi
The interface is a bit rough, and it likely won't be able to catch up due to
limitations in Minetest's modding API. For the same reason I don't know if
porting every piece in the original mod will be possible.

I plan on changing the way costs are computed (when I'll add them to the mod):
instead of being fixed, they will be paid as needed, meaning no constant
wrappers and no cost for tricks that don't get executed.

At the moment multiple inputs can share the same side, but only one arrow is
visible. I haven't decided yet wheter I'll keep allowing this (and fix the
display bug) or adopt the restriction of no more than one input on each side
(as it is in Psi).

Some pieces have slight modifications:
- the term *block* has been replaced with *node*. Technically in Minetest-speak
  Minecraft's *entities* should be called *objects*, but I think I'll leave them
  as they are;
- **Operator: Vector Piecewise Maximum** and **Minimum** optionally accept a
  third input vector;
- **Operator: Vector Multiply** and **Divide** optionally accept a second input
  number;
- **Trick: Sleep**'s input is in seconds instead of ticks.

Some pieces are meaningless in Minetest:
- There's no distinction between how long it takes to break a node and how hard
  it is to break, so **Operator: Block Hardness** and
  **Operator: Block Mining Level** have been joined in a single piece
  (**Operator: Node Hardness**).

## Adding a piece

A spell is defined by a table contained in `tau.pieces` and a function in
`tau.exec_table`:

```lua
{id = "operator_vector_raycast", category = "Operators", type = "vector",
  inputs = {
    {name = "Position", type = "vector"},
    {name = "Ray",      type = "vector"},
    {name = "Max",      type = "number", optional = true}},
  name = "Operator: Vector Raycast", desc = "Returns the position of the first non liquid node encountered by tracing a ray starting from Position with direction Ray. If nothing is found within a distance of min(Max, 64) an error is signaled"
}
```

```lua
operator_vector_raycast = function(s, p, c)
  local pos, ray, max = s[p.ins[1]].val, s[p.ins[2]].val, p.ins[3] and s[p.ins[3]].val
  for thing in raycast(pos, ray, max) do
    p.val = thing.under
    return
  end

  if not s.noerr then
    tau.hud_error(c, "No nodes found by Operator: Vector Raycast")
  end

  return true
end
```

Some things to keep in mind:

- You should check that tricks don't operate outside the 32 block range (which
  may be modified by changing the value of `tau.trick_range`). You can use
  `tau.trick_in_range(focal_point, target_point, caster, noerr)` for this:

```lua
if not tau.trick_in_range(fp, pos, c, s.noerr) then
  return true
end
```

  where `fp`, `c` and `s` are those passed as input to the function you're
  defining while `pos` is the position where the trick's effect will take place.

- As spell execution may be delayed, you should ensure that every Objref you
  take as input is valid:

```lua
if not tau.objref_valid(t) then
  return true
end
```

  If the piece is a trick use `return` instead of `return true` so that the
  spell will continue even if the trick can't be executed.

- If you need to report an error you can use `tau.hud_error(user, text)` as
  follows:

```lua
if not s.noerr then
  tau.hud_error(c, "Error text here")
end
```

## License
Files contained in `textures/` were taken from Psi and redistributed in
accordance to the [Psi License](https://psi.vazkii.net/license.php).
Everything else is licensed under the GNU General Public License version 3,
whose text you can find in `COPYING`, or (at your option) any later version of
the GNU GPL.
