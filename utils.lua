function tau.hud_error(user, text)
  minetest.after(3, function(id) user:hud_remove(id) end,
    user:hud_add {
      hud_elem_type = "text",
      position      = {x = 0.5, y = 0.6},
      text          = text,
      alignment     = {x = 0, y = 0},
      scale         = {x = 100, y = 100},
      number        = 0xFF0000
  })
end

function tau.sign(n)
  if n == 0 then
    return 0
  elseif n > 0 then
    return 1
  else
    return -1
  end
end

function tau.is_axis(v)
  return (v.x == 0 and 1 or 0) + (v.y == 0 and 1 or 0) + (v.z == 0 and 1 or 0) == 2
end

function tau.objref_valid(o)
  return o:get_pos() ~= nil
end

function tau.trick_in_range(focal_point, target_point, caster, noerr)
  if vector.distance(focal_point, target_point) > tau.trick_range then
    if not noerr then
      tau.hud_error(caster, "Spell target is outside maximum 32 node radius")
    end

    return false
  end

  return true
end

