local PROGRAM_SIDE = 9
local SPACING = 0.25
local PANEL_W = 3

local contexts = {}

local function ensure_context(pname)
  if not contexts[pname] then
    contexts[pname] = {}
  end
end

local function input_color_mod(i)
  local colors = {"red", "green", "blue", "yellow"}
  return "^[multiply:" .. colors[i]
end

-- 'piece' may either be as found in pieces.lua or the contracted version used
-- for storing the spell in the programmer
-- (e.g. {id = "constant_number", ins = {}, val = 42})
local function piece_button_formspec(x, y, piece, name, selected)
  local btn, texture = "", "tau_" .. piece.id .. ".png;"

  if piece.id == "constant_number" then
    texture = "tau_empty.png;"

    -- TODO bigger font - should be customizable in 5.3.1/5.4.0
    local xoff = 0.5 - math.log10(piece.val ~= 0 and math.abs(piece.val) or 1) / 20
    btn = "label[" .. x + xoff .. "," .. y + 0.5 ..  ";" ..  piece.val .. "]"
  end

  btn = "image_button[" ..
    x .. "," .. y .. ";1,1;" ..
    texture .. name .. ";]" ..
    btn

  if piece.id ~= "empty" then
    local p = tau.pieces_by_id[piece.id]
    btn = btn .. "tooltip[" .. name .. ";" .. p.name ..
      (p.desc and "\n" .. p.desc or "") .. "]"
  end

  if selected then
    btn = "box[" ..
      x - 0.05 .. "," ..
      y - 0.05 .. ";" ..
      "1.1,1.1;red]" ..
      btn
  end

  if piece.ins then
    local b = SPACING
    local a = 3/2 * b

    for i, side in ipairs(piece.ins) do
      local inx, iny, dim

      if side == "n" or side == "s" then
        dim = a .. "," .. b
      elseif side == "e" or side == "w" then
        dim = b .. "," .. a
      end

      if side == "n" then
        inx = x + (1 - a) / 2
        iny = y - b
      elseif side == "s" then
        inx = x + (1 - a) / 2
        iny = y + 1
      elseif side == "e" then
        inx = x + 1
        iny = y + (1 - a) / 2
      elseif side == "w" then
        inx = x - b
        iny = y + (1 - a) / 2
      end

      if dim then
        btn = btn .. "image[" .. inx .. "," .. iny .. ";" .. dim ..
          ";tau_input_" ..  side .. ".png" .. input_color_mod(i) .. "]"
      end
    end
  end

  return btn
end

local function piece_select_formspec(tab, search)
  -- tab number 1 is All
  tab = tonumber(tab)
  local cat = (tab and tab > 1) and tau.categories[tab - 1] or nil

  if search then
    search = string.lower(search)
  end

  local len = math.floor(PROGRAM_SIDE / 1.5)
  local len_spc = len * (1 + SPACING) + SPACING

  local btns, i, last_y = "", 1, 0
  for _, piece in pairs(tau.pieces) do
    if cat and piece.category ~= cat or search and
        not string.find(string.lower(piece.name or ""), search) then
      goto next
    end

    last_y = math.floor((i - 1) / len) * (1 + SPACING)
    btns = btns ..
      piece_button_formspec(
        (i - 1) % len * (1 + SPACING) + SPACING,
        last_y, piece, piece.id)
    i = i + 1

    ::next::
  end

  return
    "formspec_version[4]" ..
    "size[" .. len_spc .. "," .. len_spc .. "]" ..
    "tabheader[0,0;tab;All," .. table.concat(tau.categories, ",") .. ";" ..
      (tab or "") .. ";;]" ..
    "label[0.25,0.25;Search:]field[1,0.1;3,0.3;search;;]" ..
    "field_close_on_enter[search;false]" ..
    "scrollbaroptions[max=" .. last_y .. ";smallstep=1]" ..
    "scrollbar[0,0.5;0.2," .. len_spc .. ";vertical;scrl;]" ..
    "scroll_container[0,0.5;" .. len_spc .. "," .. len_spc ..
      ";scrl;vertical;1]" ..
    btns ..
    "scroll_container_end[]"
end

-- TODO decide if inputs may share the same side or not
-- as of now they can but only one arrow is visible if they do
local function program_formspec(program, sel_r, sel_c)
  local height = PROGRAM_SIDE + (PROGRAM_SIDE + 1) * SPACING
  local width = height + PANEL_W
  local x = height + 0.1

  local sp = "anchor[" .. height / (2 * width) .. ",0.5]" ..
    "button[" .. x .. ",0.5;" .. PANEL_W - SPACING - 0.2 .. ",0.5;check;Check]" ..
    "button[" .. x .. ",1.1;" .. PANEL_W - SPACING - 0.2 .. ",0.5;clear;Clear]"

  local panel_start_y = 2

  if program.error then
    sp = sp .. "style_type[label;textcolor=red]" ..
      "label[" .. x .. "," .. panel_start_y .. ";" ..
      program.error:gsub("]", "\\]") .. "]" ..
      "style_type[label;textcolor=white]"

    panel_start_y = panel_start_y + 0.3
  end

  for r, row in ipairs(program) do
    for c, piece in ipairs(row) do
      sp = sp .. piece_button_formspec(
        c * (1 + SPACING) - 1,
        r * (1 + SPACING) - 1,
        piece, r .. ":" .. c,
        r == sel_r and c == sel_c
      )
    end
  end

  if sel_r and sel_c then
    local sel = program[sel_r][sel_c]
    local inputs = tau.pieces_by_id[sel.id].inputs

    if inputs and sel.ins then
      for i, input in ipairs(inputs) do
        local side = sel.ins[i]
        local y = panel_start_y + 2.5 * (i - 1)
        local color = input_color_mod(i) .. ";"
        local text = input.name .. (input.optional and " (optional, " or " (") ..
          "type: " .. input.type .. ")"

        sp = sp .. "label[" .. x .. "," .. y .. ";" .. text .. "]" ..
          "image_button[" .. x + 0.5 .. "," .. y + 0.3 .. ";0.6,0.4;tau_input_n.png" ..
          (side == "n" and color or ";") .. i .. "@n;;;drawborder=false;]" ..

          "image_button[" .. x + 0.5 .. "," .. y + 1.5 .. ";0.6,0.4;tau_input_s.png" ..
          (side == "s" and color or ";") .. i .. "@s;;;drawborder=false;]" ..

          "image_button[" .. x + 1.2 .. "," .. y + 0.8 .. ";0.4,0.6;tau_input_e.png" ..
          (side == "e" and color or ";") .. i .. "@e;;;drawborder=false;]" ..

          "image_button[" .. x       .. "," .. y + 0.8 .. ";0.4,0.6;tau_input_w.png" ..
          (side == "w" and color or ";") .. i .. "@w;;;drawborder=false;]" ..

          (input.optional and
          "image_button[" .. x + 0.6 .. "," .. y + 0.9 .. ";0.4,0.4;tau_dot.png" ..
          ((not side or side == "x") and color or ";") ..
          i .. "@x;;;drawborder=false;]" or "")
      end

    elseif sel.id == "constant_number" then
      sp = sp .. "field[" .. height + 0.1 .. "," .. panel_start_y .. ";1.5,0.3;" ..
        "const_input;Constant value:;" ..  sel.val .. "]" ..
        "field_close_on_enter[const_input;false]"
    end
  end

  return
    "formspec_version[4]" ..
    "size[" .. width .. "," .. height .. "]" ..
    sp
end

local function programmer_reset(pos)
  local meta = minetest.get_meta(pos)

  local program = {}
  for i = 1, PROGRAM_SIDE do
    program[i] = {}
    for j = 1, PROGRAM_SIDE do
      program[i][j] = {id="empty"}
    end
  end
  meta:set_string("tau:program", minetest.serialize(program))
end

minetest.register_node("tau:programmer", {
  description = "Spell Programmer",
  groups = { cracky = 1 },

  on_construct = programmer_reset,

  on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
    local pname = clicker:get_player_name()
    if not pname then
      return
    end

    ensure_context(pname)
    contexts[pname].prog_pos = pos

    local program = minetest.deserialize(
      minetest.get_meta(pos):get_string("tau:program"))
    minetest.show_formspec(pname, "tau:programmer",
      program_formspec(program))
  end
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
  if formname ~= "tau:programmer" then
    return
  end

  local pname = player:get_player_name()
  ensure_context(pname)

  local sel = contexts[pname].selected
  local pos = contexts[pname].prog_pos
  if not pos then
    return
  end

  local meta = minetest.get_meta(pos)
  local program = minetest.deserialize(meta:get_string("tau:program"))

  if fields.quit then
    contexts[pname] = nil
    return
  end

  if fields.check then
    local _, err = tau.compile(program)

    if err then
      program.error = err
      minetest.show_formspec(pname, "tau:programmer",
        program_formspec(program, sel and sel[1], sel and sel[2]))
    end
  end

  if fields.clear then
    programmer_reset(pos)
    contexts[pname].selected = nil
    minetest.show_formspec(pname, "tau:programmer",
      program_formspec(minetest.deserialize(meta:get_string("tau:program"))))
    return
  end

  if fields.const_input and sel then
    local p = program[sel[1]][sel[2]]
    local n = tonumber(fields.const_input)
    if n and p.id == "constant_number" then
      p.val = n
    end

    meta:set_string("tau:program", minetest.serialize(program))
    minetest.show_formspec(pname, "tau:programmer",
      program_formspec(program, unpack(sel)))
  end

  local r, c, i, s, input
  for key, _ in pairs(fields) do
    r, c = key:match("(%d+):(%d+)")
    r, c = tonumber(r), tonumber(c)

    if r and c and r <= PROGRAM_SIDE and c <= PROGRAM_SIDE then
      if sel and sel[1] == r and sel[2] == c then
        minetest.show_formspec(pname, "tau:piece_select",
          piece_select_formspec())
      else
        contexts[pname].selected = {r, c}
        minetest.show_formspec(pname, "tau:programmer",
          program_formspec(program, r, c))
      end

      return
    end

    if sel then
      local sel_p = program[sel[1]][sel[2]]
      i, s = key:match("(%d+)@(%a)")
      i = tonumber(i)

      if sel_p.ins and i and tau.pieces_by_id[sel_p.id].inputs[i] and
        (s == "n" or s == "s" or s == "e" or s == "w" or s == "x") then

        sel_p.ins[i] = s
        meta:set_string("tau:program", minetest.serialize(program))
        minetest.show_formspec(pname, "tau:programmer",
          program_formspec(program, sel[1], sel[2]))

        return
      end
    end
  end
end)

minetest.register_on_player_receive_fields(function(player, formname, fields)
  if formname ~= "tau:piece_select" then
    return
  end

  local pname = player:get_player_name()

  ensure_context(pname)
  local pos = contexts[pname].prog_pos
  local sel = contexts[pname].selected
  local tab = contexts[pname].ps_tab

  if not (pos and sel) then
    return
  end

  if fields.quit then
    contexts[pname] = nil
    return
  end

  if fields.tab then
    contexts[pname].ps_tab = fields.tab
    minetest.show_formspec(pname, "tau:piece_select",
      piece_select_formspec(fields.tab))
    return
  end

  if fields.search then
    minetest.show_formspec(pname, "tau:piece_select",
      piece_select_formspec(tab, fields.search))
  end

  local piece
  for key, _ in pairs(fields) do
    piece = tau.pieces_by_id[key]

    if piece then
      break
    end
  end

  if piece then
    local meta = minetest.get_meta(pos)
    local program = minetest.deserialize(meta:get_string("tau:program"))

    local p = {id = piece.id}
    if piece.inputs then
      p.ins = {}
    end
    if piece.id == "constant_number" then
      p.val = 0
    end
    program[sel[1]][sel[2]] = p
    meta:set_string("tau:program", minetest.serialize(program))

    minetest.show_formspec(pname, "tau:programmer",
      program_formspec(program, unpack(sel)))
  end
end)
