tau = {}

dofile(minetest.get_modpath("tau") .. "/utils.lua")
dofile(minetest.get_modpath("tau") .. "/pieces.lua")
dofile(minetest.get_modpath("tau") .. "/spell.lua")
dofile(minetest.get_modpath("tau") .. "/programmer.lua")
dofile(minetest.get_modpath("tau") .. "/cad.lua")

-- TODO
-- infotext is used in Node Metadata to show a tooltip when hovering the
-- crosshair over a node. This is useful when showing the ownership or status
-- of a node.
--
-- description is used in ItemStack Metadata to override the description when
-- hovering over the stack in an inventory. You can use colours by encoding
-- them with minetest.colorize().
--
-- minetest.get_heat(pos)
-- minetest.get_humidity(pos)
--
--  
